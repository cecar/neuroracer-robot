# NeuroRacer
Collection of ROS packages for the Racecar

In this repository you will find the [catkin workspace](http://wiki.ros.org/catkin/workspaces) used to run the robot.
Catkin is the building tool for [ROS Projects](http://wiki.ros.org/Documentation) and ROS is the tool which was used to 
create this robot control. If you follow the instructions under [Setup](#setup) you will be able to execute the launch 
command, run the robot and control it with a controller. 

The following describes the execution workflow to start the robot. The only thing needed is to execute the `roslaunch` 
command in a terminal, your controller should already be connected with your system.

The [roslaunch](http://wiki.ros.org/roslaunch) command is the following:

```bash
roslaunch neuroracer neuroracer.launch
```

After the launch command you should be able to control the robot with your controller. This is displayed in the following
image.

![robot run workflow](images/robot_run_flow.png)
<br/>

<table>
  <tr>
    <td valign="top">
    
## Content
- [Requirements](#requirements)
- [Python Version](#python-version)
- [Setup](#setup)
- [Hardware Configuration](#hardware-configuration)
- [Manual Control](#manual-control)
- [Architecture](#architecture)
- [External Packages (sources included)](#external-packages-sources-included)
- [Add-Ons (sources not included)](#add-ons-sources-not-included)
- [Media](#media)
    </td>
  </tr>
</table>

## Requirements
* __neuroracer-robot-install__  
Follow the [`neuroracer-robot-install`](https://gitlab.com/NeuroRace/neuroracer-robot-install.git) guide to set up the Jetson TX2 board.

## Python Version

Due to [ROS](http://wiki.ros.org/Documentation), Python 2.7 is required. As soon as ROS fully supports Python3.x, the Python 2.7 support will be dropped.

## Setup 

`Note:`  
This is automatically set up when using the [neuroracer-robot-install](https://gitlab.com/NeuroRace/neuroracer-robot-install.git) Project,
you don't have to do it by yourself. The following has mostly informational purpose.

* To use this catkin workspace it is necessary to install [`neuroracer-common`](https://gitlab.com/NeuroRace/neuroracer-common.git) package via pip. Therefor clone and install the package.
```bash
git clone https://gitlab.com/NeuroRace/neuroracer-common.git
pip2 install --user -e neuroracer-common
```

* For autonomous tasks, the package [`neuroracer-ai`](https://gitlab.com/NeuroRace/neuroracer-ai.git) is required.
```bash
git clone https://gitlab.com/NeuroRace/neuroracer-ai.git
pip2 install --user -e neuroracer-ai
```

* Further, the [`neuroracer-configserver`](https://gitlab.com/NeuroRace/neuroracer-configserver.git) is used for setup and configuration handling. Clone the repository into [`src`](src/) folder and install the client.
```bash
cd src
git clone https://gitlab.com/NeuroRace/neuroracer-configserver.git
cd neuroracer-configserver
python2 setup.py install --user
```

## Hardware Configuration
To address the hardware and have no conflict, some devices require specific system configuration. An example for [udev rules](https://wiki.archlinux.org/index.php/udev) regarding Lidar, IMU and two Arduino Boards is given in [example_udev](example_udev/). For further hardware system configuration in general, see the manifactures/distribution documentation. 


## Manual Control
The controller mapping is defined in __joy_mapping.yaml__. Examples can be found [here](example_configs/joy_mapping_examples).  

__IMPORTANT:__  
The yaml file for joy mapping has to be named __joy_mapping.yaml__ (not joy_mapping_xbox360.yaml or otherwise). To steer 
or accelerate the car the __deadman__ button has to be hold down. The button mapping is displayed in the 
following image.

![alt Default Controller Layout](https://gitlab.com/NeuroRace/neuroracer-robot/raw/master/images/xbox_one_controller_layout.jpg)

## Architecture
![Robot Software Architecture](images/neuroracer-robot-architecture.png)

## External Packages (sources included)
The following package is an external one and differently licensed. It has been added as fixed version from its sources to guarentee a stable support for this project. Please check the in-package description for further information.
* [Razor IMU 9DOF](https://github.com/KristofRobot/razor_imu_9dof)  
An Inertial Measurement Unit can be optionally used. It is required as hard include from source due to code issues (2018/05/21 - fixed imu_nody.py for Python 2.7 in source), otherwise the official [ros-imu-9dof](http://wiki.ros.org/razor_imu_9dof) package could be used.

## Add-Ons (sources not included)
The following packages are not included by the project installer but supported by the neuroracer ecosystem (predefined launch commands included in [neuroracer.launch](src/neuroracer/launch/neuroracer.launch)).
* [RP-Lidar](http://wiki.ros.org/rplidar)  
Follow installation instructions on their website and clone project into [`src`](src/) folder and rebuild catkin workspace.

```
git clone https://github.com/robopeak/rplidar_ros.git
```

* [ZED Camera](http://wiki.ros.org/zed-ros-wrapper)  
Follow installation instructions on their website and clone project into [`src`](src/) folder and rebuild catkin workspace.

```
git clone https://github.com/stereolabs/zed-ros-wrapper.git
```

## Media

### SL Constant Speed - Robot (2-3 m/s)
![](http://home.htw-berlin.de/~baumapa/robot_demo.mp4)
