#!/usr/bin/env python
from neuroracer_common.run_wrapper import run_ai


def main():
    run_ai()


if __name__ == '__main__':
    main()
