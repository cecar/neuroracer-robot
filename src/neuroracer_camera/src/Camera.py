#!/usr/bin/env python

from sys import argv

import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image, CompressedImage

# neuroracer-robot
PIPE = "nvarguscamerasrc ! video/x-raw\(memory:NVMM\), format=NV12, width=1280, height=720, framerate=120/1 ! nvvidconv flip-method=0 ! video/x-raw, format=BGRx  ! videoconvert ! video/x-raw, format=BGR ! appsink"

class Camera:
    def __init__(self, queue_size=1):
        self.cv_bridge = CvBridge()

        # camera stream
        self.input_stream = cv2.VideoCapture(PIPE)
        if not self.input_stream.isOpened():
            raise Exception('Camera stream did not open\n')
            
        # publisher comprs
        self.publisher_comprs = rospy.Publisher("/nr/camera/output/compressed",
                                                CompressedImage,
                                                queue_size=queue_size)

    def calibrate_camera(self, verbose=0):
        # TODO
        # Based on:
        # http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_calib3d/py_calibration/py_calibration.html
        pass
        # import numpy as np
        # import glob
        #
        # # termination criteria
        # criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        #
        # # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        # objp = np.zeros((6 * 7, 3), np.float32)
        # objp[:, :2] = np.mgrid[0:7, 0:6].T.reshape(-1, 2)
        #
        # # Arrays to store object points and image points from all the images.
        # objpoints = []  # 3d point in real world space
        # imgpoints = []  # 2d points in image plane.
        #
        # images = glob.glob('*.jpg')
        #
        # gray = None
        # for fname in images:
        #     img = cv2.imread(fname)
        #     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #
        #     # Find the chess board corners
        #     ret, corners = cv2.findChessboardCorners(gray, (7, 6), None)
        #
        #     # TODO replace cv2.findChessboardCorners by cv2.findCirclesGrid(), since it requires less images
        #     '''
        #     See documentation for more info:
        #     http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_calib3d/py_calibration/py_calibration.html
        #     '''
        #
        #     # If found, add object points, image points (after refining them)
        #     if ret:
        #         objpoints.append(objp)
        #         cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
        #         imgpoints.append(corners)
        #
        #         # Draw and display the corners
        #         cv2.drawChessboardCorners(img, (7, 6), corners, ret)
        #         cv2.imshow('img', img)
        #         cv2.waitKey(500)
        #
        # cv2.destroyAllWindows()
        #
        # # TODO init gray?
        # if gray is not None:
        #     ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        #
        #     img = cv2.imread('left12.jpg')
        #     h, w = img.shape[:2]
        #     newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
        #
        #     # undistort method 1
        #     dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
        #
        #     # undistort method 2
        #     # mapx, mapy = cv2.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w, h), 5)
        #     # dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
        #
        #     # crop the image
        #     x, y, w, h = roi
        #     dst = dst[y:y + h, x:x + w]
        #     cv2.imwrite('calibresult.png', dst)
        #
        #     if verbose == 1:
        #         # Re-projection Error
        #         tot_error = 0
        #         for i in xrange(len(objpoints)):
        #             imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        #             error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2) / len(imgpoints2)
        #             tot_error += error
        #
        #         print "mean error: ", tot_error / len(objpoints)
        #
        #         # TODO
        #         # store the camera matrix and distortion coefficients using write functions
        #         # in Numpy (np.savez, np.savetxt etc) for future uses
        # else:
        #     raise ValueError('')

    def publish_compressed(self, verbose=0):
        while not rospy.is_shutdown() and self.input_stream.isOpened():
            success, frame = self.input_stream.read()
            msg_frame = self.cv_bridge.cv2_to_compressed_imgmsg(frame)
            self.publisher_comprs.publish(msg_frame.header, msg_frame.format, msg_frame.data)
            if verbose:
                rospy.loginfo(msg_frame.header.seq)
                rospy.loginfo(msg_frame.format)


def main():
    # default values
    calibrate = False
    verbose = 0  # use 1 for debug

    # TODO ask for calibration on script start instead of argv?
    # TODO use argparser
    '''
    if argv[1:]:
        if argv[1:][0]:
            calibrate = (argv[1:][0] == 'True')
        if argv[1:][1]:
            queue_size = argv[1:][1]
        if argv[1:][2]:
            publisher_type = argv[1:][2]
        if argv[1:][3]:
            verbose = argv[1:][3]
    '''

    try:
        # register node
        rospy.init_node('neuroracer_camera', anonymous=True)

        cam = Camera()

        if calibrate:
            cam.calibrate_camera(verbose)
            
        cam.publish_compressed(verbose)
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
