#!/usr/bin/env python

import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage


cv_bridge = CvBridge()
CAMERA_TOPIC = "/zed/left/image_raw_color/compressed"


def camera_callback(compressed_image_message):
    image = cv_bridge.compressed_imgmsg_to_cv2(compressed_image_message)
    #cv2.destroyAllWindows()
    cv2.imshow("image", image)
    cv2.waitKey(1)


def main():
    rospy.init_node('neuroracer_camera_viewer', anonymous=True)
    rospy.Subscriber(CAMERA_TOPIC, CompressedImage, camera_callback)
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        cv2.destroyAllWindows()
