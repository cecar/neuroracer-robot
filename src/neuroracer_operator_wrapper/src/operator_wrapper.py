#!/usr/bin/env python
from neuroracer_common.run_wrapper import run_operator


def main():
    run_operator()


if __name__ == '__main__':
    main()
